package br.com.bebidas.Dao;

import java.math.BigDecimal;

import org.junit.Ignore;
import org.junit.Test;

import br.com.bebidas.Dao.ProdutoDao;
import br.com.bebidas.Dao.SessaoDao;
import br.com.bebidas.Domain.Produto;
import br.com.bebidas.Domain.Sessao;

public class ProdutoDaoTest {

	@Test
	@Ignore
	public void salvar() {
		
		Produto p = new Produto();
		p.setDescricao("Refrigerante Coca-Cola 500 ml");
		p.setNome("Coca-Cola");
		p.setPreco(new BigDecimal("9.50"));
		p.setQuantidade(new Short("500"));
		p.setTipo(1);
		
		ProdutoDao pd = new ProdutoDao();
		pd.salvar(p); 
		
	}
	
}
