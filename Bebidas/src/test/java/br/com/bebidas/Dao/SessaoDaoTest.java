package br.com.bebidas.Dao;

import org.junit.Ignore;
import org.junit.Test;
import java.util.List;

import br.com.bebidas.Dao.SessaoDao;
import br.com.bebidas.Domain.Sessao;

public class SessaoDaoTest {

	@Test

	public void salvar() {
		Sessao s = new Sessao();
		for(int i =1; i<6; i++) {
			
		s.setNumero(i);
		s.setCapArmazenado(0.0);
		s.setOcupado(false);
		s.setTipoDeSessao(0);
		
		SessaoDao sd = new SessaoDao();
		sd.salvar(s);
		}
	}
	
	@Test
	@Ignore
	public void listar() {
		
		SessaoDao sd = new SessaoDao();
		
		List<Sessao> resultado	= sd.listar();
		for(Sessao sessao : resultado) {
			System.out.println("Código da Sessao: " + sessao.getCodigo());
		}
	}
	
	
	
}
