package br.com.bebidas.Bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.bebidas.Dao.ProdutoDao;
import br.com.bebidas.Domain.Produto;

@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class ProdutoBean implements Serializable {

	private Produto produto;
	private List<Produto> produtos;
	
	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public Produto getProduto() {
		return produto;
	}
	
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	@PostConstruct
	public void listar() {
		try {
			ProdutoDao pd = new ProdutoDao();
			produtos = pd.listar();
		}catch (RuntimeException erro) {
			Messages.addFlashGlobalError("Ocorreu um erro ao tentar listar as cidades");
			erro.printStackTrace();
		}
	}
	
	public void novo() {
		produto = new Produto();
	}
	
	public void salvar() {
		try {
			ProdutoDao pd = new ProdutoDao();
			pd.salvar(produto);
			novo();
			
			Messages.addGlobalInfo("Produto Salvo com Sucesso");
		}catch(RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao salvar o produto");
			erro.printStackTrace();
		}
	}
	
	public void excluir(ActionEvent evento) {
		produto = (Produto)evento.getComponent().getAttributes().get("Estado Selecionado");
		Messages.addGlobalInfo("Produto:" + produto.getNome());
	}
	
}
