package br.com.bebidas.Domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@SuppressWarnings("serial")
@Entity
public class Sessao extends GenericDomain{
	
	@Column(length = 1, nullable = false)
	private int numero;
	
	@Column(nullable = false)
	private boolean ocupado;
	
	@Column(nullable=false,precision=5, scale=2)
	private double capArmazenado;
	
	@Column
	private int tipoDeSessao;

	public int getTipoDeSessao() {
		return tipoDeSessao;
	}

	public void setTipoDeSessao(int tipoDeSessao) {
		this.tipoDeSessao = tipoDeSessao;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public boolean isOcupado() {
		return ocupado;
	}

	public void setOcupado(boolean ocupado) {
		this.ocupado = ocupado;
	}

	public double getCapArmazenado() {
		return capArmazenado;
	}

	public void setCapArmazenado(double capArmazenado) {
		this.capArmazenado = capArmazenado;
	}

	
}
