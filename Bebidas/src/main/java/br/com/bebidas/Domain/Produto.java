package br.com.bebidas.Domain;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;


@SuppressWarnings("serial")
@Entity
public class Produto extends GenericDomain {

	@Column(length=80,nullable=false)
	private String descricao;
	
	@Column(length=20,nullable=false)
	private String nome;
	
	@Column(nullable=false)
	private int tipo;
	
	@Column(nullable=false)
	private Short quantidade;
	
	@Column(nullable=false, precision=6, scale=2)
	private BigDecimal preco;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public Short getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Short quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}
	
}
